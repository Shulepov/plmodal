//
//  AlertViewController.swift
//  davinci
//
//  Created by Mihail Shulepov on 25/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

// MARK: AlertAction

public enum AlertActionStyle: Hashable, Equatable {
    case `default`
    case cancel
    case destructive
    case custom(String)
    
    public var hashValue: Int {
        switch self {
        case .default: return 5
        case .cancel: return 7
        case .destructive: return 11
        case .custom(let val): return 13 + val.hashValue
        }
    }
}

public func ==(lhs: AlertActionStyle, rhs: AlertActionStyle) -> Bool {
    switch (lhs, rhs) {
    case (.default, .default): return true
    case (.cancel, .cancel): return true
    case (.destructive, .destructive): return true
    case (.custom(let lhsVal), .custom(let rhsVal)): return lhsVal == rhsVal
    default: return false
    }
}

open class AlertAction {
    open let title: String
    open let handler: () -> Void
    open let style: AlertActionStyle
    
    open var enabled = true {
        didSet {
            self.enableStateHandler?(self.enabled)
        }
    }
    
    fileprivate var enableStateHandler: ((Bool)->())? {
        didSet {
            self.enableStateHandler?(self.enabled)
        }
    }
    
    public init( title: String, style: AlertActionStyle, handler: @escaping () -> Void = {() -> Void in}) {
        self.style = style
        self.title = title
        self.handler = handler
    }
    
    public convenience init(title: String, handler: @escaping () -> Void = {() -> Void in}) {
        self.init(title: title, style: .default, handler: handler)
    }
    
    open class func cancel(title: String, handler: @escaping () -> Void = {() -> Void in}) -> AlertAction {
        return AlertAction(title: title, style: .cancel, handler: handler)
    }
    
    open class func destruct(title: String, handler: @escaping () -> Void = {() -> Void in}) -> AlertAction {
        return AlertAction(title: title, style: .destructive, handler: handler)
    }
    
    open class func custom(title: String, style: String, handler: @escaping () -> Void = {() -> Void in}) -> AlertAction {
        return AlertAction(title: title, style: .custom(style), handler: handler)
    }
}


/// Alert Button Provider
/// Responsible for customizing buttons in alert view (custom classes, more flexible appearance customization, etc...)

public protocol AlertButton: class {
    var enabled: Bool { get set }
    func setTitle(_ title: String?)
    func setImage(_ image: UIImage?)
    func setAction(_ action: @escaping () -> ())
    
    func asUIView() -> UIView
}

public protocol AlertButtonProvider {
    func createAlertButtonForActionStyle(_ actionStyle: AlertActionStyle) -> AlertButton
}


// MARK: AlertViewController

open class AlertViewController: UIViewController {
    public struct Appearance {
        public static var defaultXib: String? = nil
        
        public var titleFont: UIFont?
        public var titleColor: UIColor?
        
        public var messageFont: UIFont?
        public var messageColor: UIColor?
        
        public var backgroundColor: UIColor? = nil
        public var interButtonsMargins: CGFloat = 1 / UIScreen.main.scale
        public var buttonsEdgeInsets = UIEdgeInsets(top: 1 / UIScreen.main.scale, left: 0, bottom: 0, right: 0)
        
        public var width: CGFloat = 274
        public var allowCloseOnTouchOutside = true
        public var forcedButtonsLayoutStyle: ButtonsLayoutStyle? = nil
        public var showShadow: Bool = false
        
        public var cornerRadius: CGFloat? = nil
        
        public var buttonsHeight: CGFloat = 44
        
        public var allowAsymmetricButtonsWidth: Bool = false
        
        public var buttonsProvider: AlertButtonProvider = DefaultAlertButtonProvider()
    }
    
    open static var appearanceProxy = AlertViewController.Appearance()
    
    public enum ButtonsLayoutStyle {
        case vertical
        case horizontal
    }
    
    open var appearance = AlertViewController.appearanceProxy
    
    fileprivate var actions = [AlertAction]()             //actions for that alert
    fileprivate var contentViews = [(UIView, CGFloat)]()  //list of all content views to be added + side spacings
    
    open var didDismissAction: ((Void) -> Void)?    //action that will be called upon alert dismissing
    
    @IBOutlet open var buttonsContainer: UIView!    //container for buttons
    @IBOutlet open var contentContainer: UIView!    //contains for content
    @IBOutlet open var titleLabel: UILabel!         //title
    @IBOutlet open var backgroundView: UIView!      //backgroundView, if not specified then backgroundView=self.view
    
    public init() {
        if let xib = AlertViewController.Appearance.defaultXib {
            super.init(nibName: xib, bundle: nil)
        } else {
            let defaultXibName = "DefaultAlertViewController"
            let moduleBundle = Bundle(for: AlertViewController.self)
            if let resourcesBundlePath = moduleBundle.path(forResource: "PLModal", ofType: "bundle") {
                let resourcesBundle = Bundle(path: resourcesBundlePath)
                super.init(nibName: defaultXibName, bundle: resourcesBundle)
            } else {
                super.init(nibName: defaultXibName, bundle: moduleBundle)
            }
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle? = nil) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        if self.backgroundView == nil {
            self.backgroundView = self.view
        }
        
        if let background = self.backgroundView {
            if let backgroundColor = self.appearance.backgroundColor {
                background.backgroundColor = backgroundColor
            }
            background.layer.cornerRadius = self.appearance.cornerRadius ?? 0
        }
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.didDismissAction?()
    }
    
    open func didLayoutButtons(_ buttons: [UIView], inView view: UIView, withStyle style: ButtonsLayoutStyle) {
        
    }
    
    @IBAction
    open func dismiss() {
        PLModal.shared.dismissAlertViewController(self)
    }
    
    open override var title: String? {
        didSet {
            if let titleLabel = self.titleLabel {
                titleLabel.text = self.title
            }
        }
    }
    
    open var attributedTitle: NSAttributedString? {
        didSet {
            self.title = self.attributedTitle?.string
            if let titleLabel = self.titleLabel {
                titleLabel.attributedText = self.attributedTitle
            }
        }
    }
    
    @discardableResult
    open func addMessage(_ message: String) -> UITextView {
        let textView = UITextView()
        textView.text = message
        if let color = self.appearance.messageColor {
            textView.textColor = color
        }
        if let font = self.appearance.messageFont {
            textView.font = font
        }
        self.setupTextView(textView)
        self.addContentView(textView, sideSpacing: 4)
        return textView
    }
    
    @discardableResult
    open func addAttributedMessage(_ attributedText: NSAttributedString) -> UITextView {
        let textView = UITextView()
        textView.attributedText = attributedText
        self.setupTextView(textView)
        self.addContentView(textView, sideSpacing: 4)
        return textView
    }
    
    fileprivate func setupTextView(_ textView: UITextView) {
        textView.isScrollEnabled = false
        textView.textAlignment = .center
        textView.backgroundColor = UIColor.clear
        textView.isEditable = false
        textView.isSelectable = false
    }
    
    @discardableResult
    open func addTextField() -> UITextField {
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.line
        
        let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .greaterThanOrEqual,
            toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
        heightConstraint.priority = 960
        textField.addConstraint(heightConstraint)
        
        self.addContentView(textField, sideSpacing: 8)
        return textField
    }
    
    open func addSpacing(_ spacing: CGFloat) {
        let spacingView = UIView()
        let heightConstraint = NSLayoutConstraint(item: spacingView, attribute: .height, relatedBy: .equal,
            toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: spacing)
        spacingView.addConstraint(heightConstraint)
        self.addContentView(spacingView)
    }
    
    //view must have height constraint (or intrinsic height)
    open func addContentView(_ view: UIView, sideSpacing: CGFloat = 0) {
        self.contentViews.append((view, sideSpacing))
    }
    
    @discardableResult
    open func addContentViewFromNib<T: UIView>(_ nibFile: String, bundle: Bundle = Bundle.main) -> T! {
        let nibViews = bundle.loadNibNamed(nibFile, owner: self, options: nil)
        if let contentView = nibViews?.first as? T {
            if contentView == self.view {
                NSLog("Remove outlet to owner's view from next xib: \(nibFile)")
                return nil
            }
            self.addContentView(contentView)
            return contentView
        }
        NSLog("No UIViews in xib: %@", nibFile)
        return nil
    }
    
    open func addActions(_ actions: [AlertAction]) {
        self.actions += actions
    }
    
    open func addActions(_ actions: AlertAction...) {
        self.actions += actions
    }
    
    open func addAction(_ action: AlertAction) {
        self.actions.append(action)
    }
    
    open func show() {
        let _ = self.view //force load view
        self.configureTitleLabel()
        self.fillContentContainer()
        self.createAndLayoutButtons()
        
        // Constrain width
        let widthConstraint = NSLayoutConstraint(item: self.view, attribute: .width,
            relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1, constant: self.appearance.width)
        widthConstraint.priority = 990 //if some content view(s) has required Width constraint
        self.view.addConstraint(widthConstraint)
        
        PLModal.shared.presentAlertViewController(self) { config in
            if let overrideRadius = self.appearance.cornerRadius {
                config.cornerRadius = overrideRadius
            }
            config.showShadow = self.appearance.showShadow
            config.closeOnTouchOutside = self.appearance.allowCloseOnTouchOutside
        }
    }
    
    fileprivate func configureTitleLabel() {
        if self.titleLabel == nil {
            return
        }
        
        self.titleLabel.setContentCompressionResistancePriority(1000, for: .vertical)

        if let attributedTitle = self.attributedTitle {
            self.titleLabel.attributedText = attributedTitle
            
        } else if let simpleTitle = self.title {
            self.titleLabel.text = simpleTitle
            if let titleFont = self.appearance.titleFont {
                self.titleLabel.font = titleFont
            }
            if let titleColor = self.appearance.titleColor {
                self.titleLabel.textColor = titleColor
            }
            
        } else {
            self.titleLabel.text = ""
        }
    }
    
    fileprivate func fillContentContainer() {
        var prevContentView: UIView?
        for (contentView, spacing) in self.contentViews {
            contentView.translatesAutoresizingMaskIntoConstraints = false
            self.contentContainer.addSubview(contentView)
            contentView.setContentCompressionResistancePriority(1000, for: .vertical)
            
            if let prevContentView = prevContentView {
                //constrain margin between content views
                let spacingConstraint = NSLayoutConstraint(item: prevContentView, attribute: .bottom, relatedBy: .equal,
                    toItem: contentView, attribute: .top, multiplier: 1, constant: 0)
                self.contentContainer.addConstraint(spacingConstraint)
            } else {
                //constrain to top
                let topSpaceConstraint = NSLayoutConstraint(item: self.contentContainer, attribute: .top, relatedBy: .equal,
                    toItem: contentView, attribute: .top, multiplier: 1, constant: 0)
                self.contentContainer.addConstraint(topSpaceConstraint)
            }
            
            //side constraints
            let leftSpacing = NSLayoutConstraint(item: contentView, attribute: .left, relatedBy: .equal,
                toItem: self.contentContainer, attribute: .left, multiplier: 1, constant: spacing)
            let rightSpacing = NSLayoutConstraint(item: self.contentContainer, attribute: .right, relatedBy: .equal,
                toItem: contentView, attribute: .right, multiplier: 1, constant: spacing)
            self.contentContainer.addConstraints([leftSpacing, rightSpacing])
            
            prevContentView = contentView
        }
        
        //constain to bottom
        if let lastContentView = prevContentView {
            let bottomSpaceConstraint = NSLayoutConstraint(item: self.contentContainer, attribute: .bottom, relatedBy: .equal,
                toItem: lastContentView, attribute: .bottom, multiplier: 1, constant: 0)
            self.contentContainer.addConstraint(bottomSpaceConstraint)
        }
    }
    
    /// If specified appearance.forcedButtonsLayoutStyle - return it
    /// If there are > 2 buttons - return vertical
    /// If there are 2 buttons and sum of their titles less than 25 symbols - return horizontal
    fileprivate func layoutStyleForButtons(_ buttons: [AlertAction]) -> ButtonsLayoutStyle {
        if let style = self.appearance.forcedButtonsLayoutStyle {
            return style
        }
        if buttons.count == 2 {
            if self.appearance.allowAsymmetricButtonsWidth {
                let maxLettersAmount = Int(self.appearance.width / 10)
                return buttons.reduce(0, { acc, buttonItem -> Int in
                    return acc + buttonItem.title.utf16.count
                }) > maxLettersAmount ? .vertical : .horizontal
            } else {
                return buttons.filter { button -> Bool in
                    return button.title.utf16.count > 10
                }.isEmpty ? .horizontal : .vertical
            }
        }
        return .vertical
    }
    
    /// Create and layout buttons inside container view, and return that container
    /// Horizontal Layout: H:|-button-button-button|
    /// Vertical Layout: V:|button-button-button...|
    fileprivate func createAndLayoutButtons() {
        let buttonsHolder = self.buttonsContainer!
        
        let buttonViews = self.actions.map { alertAction -> UIView in
            let button = self.appearance.buttonsProvider.createAlertButtonForActionStyle(alertAction.style)
            button.setTitle(alertAction.title)
            button.setAction { [weak self] in
                self?.onButtonClicked(alertAction); return
            }
            alertAction.enableStateHandler = { enabled in
                button.enabled = enabled
            }
            let buttonView = button.asUIView()
            buttonView.translatesAutoresizingMaskIntoConstraints = false
            buttonsHolder.addSubview(buttonView)
            
            return buttonView
        }
        
        let edgeInsets = self.appearance.buttonsEdgeInsets
        let interButtonsMargins = self.appearance.interButtonsMargins
        let buttonsHeight = self.appearance.buttonsHeight
        let hasIntrinsicSize: Bool
        if let button = buttonViews.first {
            hasIntrinsicSize = button.intrinsicContentSize.width != UIViewNoIntrinsicMetric
        } else {
            hasIntrinsicSize = false
        }
        
        let laytousStyle = self.layoutStyleForButtons(self.actions)
        switch laytousStyle {
        case .vertical:
            var previousButton: UIView?
            for button in buttonViews {
                var mapping = ["button": button]
                let hformat = "H:|-(\(edgeInsets.left))-[button]-(\(edgeInsets.right))-|"
                var vformat: String!
                if let prevButton = previousButton {
                    mapping["prevbutton"] = prevButton
                    vformat = "V:[prevbutton]-(\(interButtonsMargins))-[button(==\(buttonsHeight))]"
                } else {
                    vformat = "V:|-(\(edgeInsets.top))-[button(==\(buttonsHeight))]"
                }
                let formats = [hformat, vformat!]
                buttonsHolder.addConstraints(NSLayoutConstraint.constraintsWithVisualFormats(formats, views: mapping))
                previousButton = button
            }
            if let lastButton = previousButton {
                let constraint = NSLayoutConstraint(item: buttonsHolder, attribute: .bottom, relatedBy: .equal,
                    toItem: lastButton, attribute: .bottom, multiplier: 1.0, constant: edgeInsets.bottom)
                buttonsHolder.addConstraint(constraint)
            }
            
        case .horizontal:
            //calculate added insets in case of asymmetric buttons layout
            let buttonsContentInsets: CGFloat
            if self.appearance.allowAsymmetricButtonsWidth && hasIntrinsicSize {
                let totalButtonsWidth = buttonViews.reduce(0.0) { width, view -> CGFloat in
                    return width + view.intrinsicContentSize.width
                }
                buttonsContentInsets = (self.appearance.width - totalButtonsWidth) / CGFloat(buttonViews.count)
            } else {
                buttonsContentInsets = 0
            }
            var previousButton: UIView?
            for button in buttonViews {
                var mapping = ["button": button]
                let vformat = "V:|-(\(edgeInsets.top))-[button(==\(buttonsHeight))]-(\(edgeInsets.bottom))-|"
                var hformat: String!
                if let prevButton = previousButton {
                    mapping["prevbutton"] = prevButton
                    if self.appearance.allowAsymmetricButtonsWidth && hasIntrinsicSize {
                        let prevButtonNaturalWidth = prevButton.intrinsicContentSize.width
                        let buttonNaturalWidth = button.intrinsicContentSize.width
                        let aspect = (prevButtonNaturalWidth + buttonsContentInsets) / (buttonNaturalWidth + buttonsContentInsets)
                        hformat = "H:[prevbutton]-(\(interButtonsMargins))-[button]"
                        let widthConstraint = NSLayoutConstraint(item: prevButton, attribute: .width, relatedBy: .equal,
                            toItem: button, attribute: .width, multiplier: aspect, constant: 0)
                        buttonsHolder.addConstraint(widthConstraint)
                        
                    } else {
                        hformat = "H:[prevbutton]-(\(interButtonsMargins))-[button(==prevbutton)]"
                    }
                    
                } else {
                    hformat = "H:|-(\(edgeInsets.left))-[button]"
                }
                let formats = [hformat!, vformat]
                buttonsHolder.addConstraints(NSLayoutConstraint.constraintsWithVisualFormats(formats, views: mapping))
                previousButton = button
            }
            if let lastButton = previousButton {
                let constraint = NSLayoutConstraint(item: buttonsHolder, attribute: .right, relatedBy: .equal,
                    toItem: lastButton, attribute: .right, multiplier: 1.0, constant: edgeInsets.right)
                buttonsHolder.addConstraint(constraint)
            }
        }
        self.didLayoutButtons(buttonViews, inView: buttonsHolder, withStyle: laytousStyle)
    }
    
    func onButtonClicked(_ button: AlertAction) {
        button.handler()
        self.dismiss()
    }   
}


// MARK: make UIButton conform to AlertButton

open class AlertUIButton: UIView, AlertButton {
    fileprivate let button: UIButton
    fileprivate var callback: (() -> ())?
    
    public init(button: UIButton) {
        self.button = button
        super.init(frame: button.frame)
        button.frame = self.bounds
        button.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        button.translatesAutoresizingMaskIntoConstraints = true
        self.addSubview(button)
    }
    
    open override func invalidateIntrinsicContentSize() {
        button.invalidateIntrinsicContentSize()
    }
    
    open override var intrinsicContentSize : CGSize {
        return button.intrinsicContentSize
    }

    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setTitle(_ title: String?) {
        self.button.setTitle(title, for: UIControlState())
    }
    
    open func setImage(_ image: UIImage?) {
        self.button.setImage(image, for: UIControlState())
    }
    
    open func setAction(_ action: @escaping () -> ()) {
        self.callback = action
        self.button.addTarget(self, action: #selector(AlertUIButton.onClick), for: .touchUpInside)
    }
    
    open func asUIView() -> UIView {
        return self
    }
    
    open var enabled: Bool {
        get { return self.button.isEnabled }
        set { self.button.isEnabled = newValue }
    }
    
    internal func onClick() {
        self.callback?()
    }
}


/// MARK: Default Alert Button Provider

open class UIButtonAlertButtonProvider: AlertButtonProvider {
    open let configurator: ((UIButton, AlertActionStyle)->())?
    
    public init(_ configurator: ((UIButton, AlertActionStyle) -> ())? = nil) {
        self.configurator = configurator
    }
    
    open func createAlertButtonForActionStyle(_ actionStyle: AlertActionStyle) -> AlertButton {
        let button = UIButton()
        button.titleLabel?.numberOfLines = 1
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.lineBreakMode = .byClipping
        
        self.configurator?(button, actionStyle)
        return AlertUIButton(button: button)
    }
    
    open class func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}

open class DefaultAlertButtonProvider: UIButtonAlertButtonProvider {
    public init() {
        super.init({ button, actionStyle in
            DefaultAlertButtonProvider.configureButton(button, forActionStyle: actionStyle)
        })
    }
    
    fileprivate class func configureButton(_ button: UIButton, forActionStyle actionStyle: AlertActionStyle) {
        let titleFont: UIFont
        let normalTitleColor: UIColor
        
        switch actionStyle {
        case .custom(_):
            fallthrough
        case .default:
            titleFont = UIFont.systemFont(ofSize: 16)
            normalTitleColor = UIColor(red: 25.0/255, green: 133.0/255, blue: 252.0/255, alpha: 1.0)
            
        case .cancel:
            titleFont = UIFont.boldSystemFont(ofSize: 16)
            normalTitleColor = UIColor(red: 25.0/255, green: 133.0/255, blue: 252.0/255, alpha: 1.0)
            
        case .destructive:
            titleFont = UIFont.systemFont(ofSize: 16)
            normalTitleColor = UIColor(red: 255.0/255, green: 56.0/255, blue: 36.0/255, alpha: 1.0)
        }
        
        let disabledTitleColor = UIColor(white: 130.0/255, alpha: 1.0)
        
        button.titleLabel?.font = titleFont
        button.setTitleColor(normalTitleColor, for: UIControlState())
        button.setTitleColor(disabledTitleColor, for: .disabled)
        
        let normalBackgroundColor = UIColor(white: 1, alpha: 0.75)
        let highlightedBackgroundColor = UIColor(white: 230.0/255, alpha: 0.75)
        
        let normalBackground = UIButtonAlertButtonProvider.imageWithColor(normalBackgroundColor)
        let highlightedBackground = UIButtonAlertButtonProvider.imageWithColor(highlightedBackgroundColor)
        
        button.setBackgroundImage(normalBackground, for: UIControlState())
        button.setBackgroundImage(normalBackground, for: .disabled)
        button.setBackgroundImage(highlightedBackground, for: UIControlState.highlighted)
    }
}

