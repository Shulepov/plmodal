//
//  PLModal.swift
//  PLModal
//
//  Created by Mihail Shulepov on 20/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

//Window
// -View
//   -BackgroundView
//   -ContainerView
//     -HolderView (first alert)
//     -HolderView (second alert)
//     -HolderView (third alert)

class ModalViewController: UIViewController {
    fileprivate var window: UIWindow!
    fileprivate var containerViewBottomMargin: NSLayoutConstraint!
    fileprivate var containerView: UIView!
    fileprivate var outsideTapsGestureRecognizer: UITapGestureRecognizer!
    
    var contentStack = Array<PLModal.DisplayConfig>()
    
    var backgroundView: UIView!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        window = UIWindow(frame: UIScreen.main.bounds)
        window.isOpaque = false
        window.windowLevel = UIWindowLevelAlert
        window.rootViewController = self
        
        self.view.backgroundColor = UIColor.clear
        
        if self.backgroundView == nil {
            self.backgroundView = UIView(frame: self.view.bounds)
            self.backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.backgroundView.translatesAutoresizingMaskIntoConstraints = true
            self.backgroundView.alpha = 0.0
            self.view.addSubview(self.backgroundView)
        }

        self.containerView = self.createContainerView()
        
        outsideTapsGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ModalViewController.didTapOutside))
        outsideTapsGestureRecognizer.delegate = self
        self.containerView.addGestureRecognizer(outsideTapsGestureRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ModalViewController.keyboardWillAppear(_:)),
            name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ModalViewController.keyboardWillDisappear(_:)),
            name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func createContainerView() -> UIView {
        let containerView = UIView(frame: self.view.bounds)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(containerView)
        
        // Container view matches screen size, but if keyboard appears - make margin from bottom
        self.containerViewBottomMargin =
            NSLayoutConstraint.equality(view: self.view, secondView: containerView, attribute: NSLayoutAttribute.bottom)
        self.view.addConstraints([
            NSLayoutConstraint.equality(view: containerView, secondView: self.view, attribute: NSLayoutAttribute.top),
            NSLayoutConstraint.equality(view: containerView, secondView: self.view, attribute: NSLayoutAttribute.left),
            NSLayoutConstraint.equality(view: containerView, secondView: self.view, attribute: NSLayoutAttribute.right),
            self.containerViewBottomMargin
        ])
        return containerView
    }
    
    fileprivate func wrapContentView(_ config: PLModal.DisplayConfig) -> UIView {
        let holder = UIView()
        holder.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(holder)
        let contentView = config.view
        if config.cornerRadius > 0 {
            contentView.layer.cornerRadius = config.cornerRadius
            contentView.layer.masksToBounds = true
        }
        
        if config.showShadow {
            let shadowView = UIView()
            shadowView.translatesAutoresizingMaskIntoConstraints = false
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOpacity = 1
            shadowView.layer.shadowRadius = 11
            shadowView.layer.shadowOffset = CGSize.zero
            shadowView.backgroundColor = UIColor.black
            shadowView.layer.rasterizationScale = 1
            shadowView.layer.shouldRasterize = true
            shadowView.layer.cornerRadius = config.cornerRadius
            holder.addSubview(shadowView)
            
            let attributes = [NSLayoutAttribute.top, .left, .right, .bottom]
            holder.addConstraints(
                NSLayoutConstraint.equality(view: holder, secondView: shadowView, attributes: attributes)
            )
        }
        
        // Holder intended to be less than ContainerView, so it's size may be less than size of ContentView
        // If height priority of ContentView < 950 then it can adjust it's size to fit inside screen
        contentView.translatesAutoresizingMaskIntoConstraints = false
        holder.addSubview(contentView)
        holder.addConstraints([
            NSLayoutConstraint.equality(view: holder, secondView: contentView, attribute: .top),
            NSLayoutConstraint.equality(view: holder, secondView: contentView, attribute: .left),
            NSLayoutConstraint.equality(view: holder, secondView: contentView, attribute: .right, priority: 950),
            NSLayoutConstraint.equality(view: holder, secondView: contentView, attribute: .bottom, priority: 950)
        ])
        
        let widthLess = NSLayoutConstraint(item: holder, attribute: .width, relatedBy: .lessThanOrEqual,
            toItem: self.containerView, attribute: .width, multiplier: 1, constant: 0)
        let heightLess = NSLayoutConstraint(item: holder, attribute: .height, relatedBy: .lessThanOrEqual,
            toItem: self.containerView, attribute: .height, multiplier: 1, constant: 0)
        self.containerView.addConstraints([widthLess, heightLess])
        
        return holder
    }
    
    func presentContentViewController(_ config: PLModal.DisplayConfig) {
        if !self.isViewLoaded {
            let _ = self.view
        }
        if self.contentStack.isEmpty {
            self.backgroundView.backgroundColor = config.blackoutColor
            window.makeKeyAndVisible()
        }

        if let previousVCContentView = (self.contentStack.last?.view)?.superview {
            previousVCContentView.isUserInteractionEnabled = false
            previousVCContentView.layer.allowsGroupOpacity = true
            UIView.animate(withDuration: 0.2, animations: {
                previousVCContentView.alpha = 0.0
                previousVCContentView.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
            })
        }
        
        self.backgroundView.layer.removeAllAnimations()
        UIView.animate(withDuration: 0.25, animations: {
            self.backgroundView.backgroundColor = config.blackoutColor
            self.backgroundView.alpha = 1.0
        })
        
        // ContentView is centered in ContainerView
        let contentView = self.wrapContentView(config)
        containerView.addConstraints( NSLayoutConstraint.equalCenter(view: contentView, secondView: containerView))
        
        // Add new ViewController to stack and animate appear
        self.contentStack.append(config)
        self.addChildViewController(config.viewController)
        contentView.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        contentView.transform = CGAffineTransform(scaleX: 0.05, y: 0.05)
        contentView.alpha = 0.0
        UIView.animate(withDuration: 0.45, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIViewAnimationOptions(), animations: {
                contentView.transform = CGAffineTransform.identity
                contentView.alpha = 1.0
            }) { completed in
                if completed {
                    config.viewController.didMove(toParentViewController: self)
                }
            }
    }
    
    func dismissContentViewController(_ contentViewController: UIViewController) {
        if let topVCConfig = self.contentStack.last {
            let topVC = topVCConfig.viewController
            if topVC == contentViewController {
                // Hide currently visible top view controller
                self.contentStack.removeLast()
                topVC.view.isUserInteractionEnabled = false
                
                let contentView = topVC.view.superview!
                topVC.willMove(toParentViewController: nil)
                UIView.animate(withDuration: 0.23, animations: {
                    contentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                    contentView.alpha = 0.0
                }, completion: { completed in
                    contentView.removeFromSuperview()
                    topVC.removeFromParentViewController()
                })
                
                // show previos VC if available, or hide overlay
                if let previousVCContentView = (self.contentStack.last?.view)?.superview {
                    UIView.animate(withDuration: 0.3, delay: 0.12, options: UIViewAnimationOptions.curveLinear, animations: {
                        previousVCContentView.alpha = 1.0
                        previousVCContentView.transform = CGAffineTransform.identity;
                    }, completion: { completed in
                        previousVCContentView.isUserInteractionEnabled = true
                    })
                } else {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.backgroundView.alpha = 0.0
                    }, completion: { completed in
                        if self.contentStack.isEmpty {
                            self.window.isHidden = true
                        }
                    }) 
                }
                
                //disable container interaction during hiding (to prevent double taps and unwanted taps)
                outsideTapsGestureRecognizer.isEnabled = false
                let dispatchTime = DispatchTime.now() + Double(Int64(NSEC_PER_SEC * 1)) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: dispatchTime) { [weak self] in
                    if let strongSelf = self {
                        strongSelf.outsideTapsGestureRecognizer.isEnabled = true
                    }
                }
                
            } else if let index = (self.contentStack.map{ $0.viewController }.index(of: contentViewController)) {
                self.contentStack.remove(at: index)
            }
        }
    }
    
    dynamic func didTapOutside() {
       if let topVC = self.contentStack.last {
          if topVC.closeOnTouchOutside {
              self.dismissContentViewController(topVC.viewController)
          }
       }
    }
    
    dynamic func keyboardWillAppear(_ notification: Notification) {
        if self.window.isHidden {
            return
        }
        let info = notification.userInfo!
        let size = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let endSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let height = max(size.height, endSize.height)

        let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        var curve = UIViewAnimationCurve.easeInOut
        (info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).getValue(&curve)
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(duration)
        UIView.setAnimationCurve(curve)
        self.containerViewBottomMargin.constant = height
        self.containerView.superview?.layoutIfNeeded()
        UIView.commitAnimations()
    }
    
    dynamic func keyboardWillDisappear(_ notification: Notification) {
        if self.window.isHidden {
            return
        }
        let info = notification.userInfo!
        let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        var curve = UIViewAnimationCurve.easeInOut
        (info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).getValue(&curve)

        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(duration)
        UIView.setAnimationCurve(curve)
        self.containerViewBottomMargin.constant = 0
        self.containerView.superview?.layoutIfNeeded()
        UIView.commitAnimations()
    }
}

extension ModalViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == self.containerView
    }
}

open class PLModal: NSObject {
    private var rootVC: ModalViewController
    open var cornerRadius: CGFloat = 3.0
    open var showShadow = true
    open var closeOnTouchOutside = true
    open var blackoutColor = UIColor(white: 0, alpha: 0.3)
    
    open class var shared: PLModal {
        struct Static {
            static var instance = PLModal()
        }
        return Static.instance
    }
    
    override init() {
        rootVC = ModalViewController()
    }
    
    open func presentAlertViewController(_ vc: UIViewController) {
        if vc.view.superview != nil {
            NSLog("Content's view already presented in view hierarchy")
            return
        }

        self.presentAlertViewController(vc, configuration: { config in
        })
    }
    
    open func presentAlertViewController(_ vc: UIViewController, configuration: (DisplayConfig) -> ()) {
        if vc.parent != nil {
            NSLog("View controller was already presented modally!")
            return
        }
        let config = DisplayConfig(viewController: vc)
        config.showShadow = self.showShadow
        config.cornerRadius = self.cornerRadius
        config.closeOnTouchOutside = self.closeOnTouchOutside
        config.blackoutColor = self.blackoutColor
        configuration(config)
        rootVC.presentContentViewController(config)
    }
    
    open func dismissAlertViewController(_ vc: UIViewController) {
        self.rootVC.dismissContentViewController(vc)
    }
    
    open class DisplayConfig {
        open let viewController: UIViewController
        open var closeOnTouchOutside = true
        open var cornerRadius: CGFloat = 3.5
        open var showShadow = true
        open var blackoutColor = UIColor(white: 0, alpha: 0.3)
        
        open var view: UIView {
            return viewController.view
        }
        
        fileprivate init(viewController: UIViewController) {
            self.viewController = viewController
        }
    }
}
