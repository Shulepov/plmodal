//
//  NSLayoutConstraint+Convenience.swift
//  Mystery
//
//  Created by Mihail Shulepov on 28/01/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

internal extension NSLayoutConstraint {
    class func equality(view: UIView, secondView: UIView, attribute: NSLayoutAttribute, priority: UILayoutPriority = 1000) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: view, attribute: attribute, relatedBy: .equal,
            toItem: secondView, attribute: attribute, multiplier: 1, constant: 0)
        constraint.priority = priority
        return constraint
    }
    
    class func equality(view: UIView, secondView: UIView, attributes: [NSLayoutAttribute], priority: UILayoutPriority = 1000) -> [NSLayoutConstraint] {
        return attributes.map {
            return self.equality(view: view, secondView: secondView, attribute: $0, priority: priority)
        }
    }
    
    class func equalSize(view: UIView, secondView: UIView, priority: UILayoutPriority = 1000) -> [NSLayoutConstraint] {
        let attrs = [NSLayoutAttribute.width, .height]
        return equality(view: view, secondView: secondView, attributes: attrs, priority: priority)
    }
    
    class func equalCenter(view: UIView, secondView: UIView, priority: UILayoutPriority = 1000) -> [NSLayoutConstraint] {
        let attrs = [NSLayoutAttribute.centerX, .centerY]
        return equality(view: view, secondView: secondView, attributes: attrs, priority: priority)
    }
    
    class func equalSides(view: UIView, secondView: UIView, priority: UILayoutPriority = 1000) -> [NSLayoutConstraint] {
        let attrs = [NSLayoutAttribute.top, .left, .bottom, .right]
        return equality(view: view, secondView: secondView, attributes: attrs, priority: priority)
    }
    
    class func constraintsWithVisualFormats(_ formats: [String], views: [String: UIView],
        options: NSLayoutFormatOptions = []) -> [NSLayoutConstraint] {
        return formats.map { format -> [NSLayoutConstraint] in
            return NSLayoutConstraint.constraints(withVisualFormat: format, options: options, metrics: nil, views: views)
        }.reduce( [NSLayoutConstraint](), { acc, constraints in
            return acc + constraints
        })
    }
}
