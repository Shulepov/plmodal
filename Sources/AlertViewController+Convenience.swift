//
//  AlertViewController+Convenience.swift
//  PLModal
//
//  Created by Mikhail Shulepov on 04/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

public extension AlertViewController {
    public func showWithTitle(_ title: String, message: String, actions: [AlertAction] = []) {
        self.title = title
        self.addMessage(message)
        self.addActions(actions)
        self.show()
    }
    
    public func showWithTitle(_ title: String, attributedMessage: NSAttributedString, actions: [AlertAction] = []) {
        self.title = title
        self.addAttributedMessage(attributedMessage)
        self.addActions(actions)
        self.show()
    }
    
    public func showWithTitle(_ title: String, message: String, actions: AlertAction...) {
        self.title = title
        self.addMessage(message)
        self.addActions(actions)
        self.show()
    }
    
    public func showWithTitle(_ title: String, attributedMessage: NSAttributedString, actions: AlertAction...) {
        self.title = title
        self.addAttributedMessage(attributedMessage)
        self.addActions(actions)
        self.show()
    }
}
