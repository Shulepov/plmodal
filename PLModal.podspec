Pod::Spec.new do |s|

  s.name         = "PLModal"
  s.version      = "0.7.3"
  s.summary      = "Easy alert View Controllers"

  s.description  = <<-DESC
                   Easy replacement for default UIAlertView. Or another alerts.
                   DESC

  s.homepage     = "https://bitbucket.org/Shulepov/plmodal"
  s.license      = "MIT"
  s.author       = { "Shulepov Mikhail" => "shulepov.mikhail@gmail.com" }

  s.ios.deployment_target = "8.0"
  
  s.module_name  = "PLModal"

  s.source = { :git => "git@bitbucket.org:Shulepov/plmodal.git", :tag => "#{s.version}" }
  s.source_files  = "Sources/*.swift"

  s.resources = ['Sources/DefaultAlertViewController.xib']

  s.requires_arc = true

end