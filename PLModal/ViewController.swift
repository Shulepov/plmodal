//
//  ViewController.swift
//  PLModal
//
//  Created by Mikhail Shulepov on 11/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        PLModal.shared.blackoutColor = UIColor(white: 0, alpha: 0.3)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureAppearance(_ appearance: AlertViewController.Appearance) {
    }
    
    @IBAction func showSimpleAlert() {
        let alert = AlertViewController()
        alert.appearance.allowAsymmetricButtonsWidth = true
        //alert.appearance.forcedButtonsLayoutStyle = .Horizontal
        alert.title = "Choose wisely"
        
        alert.addMessage("Really delete all of your important information?")
       /* let textField = alert.addTextField()
        textField.placeholder = "Sample"

        alert.addMessage("Second message")
        let nextTF = alert.addTextField()
        nextTF.placeholder = "Next after spacing"*/

        alert.addAction(AlertAction(title: "Rej", style: .cancel))
        alert.addAction(AlertAction(title: "Accept the chal", style: .destructive))
        alert.addAction(AlertAction(title: "Acc"))
/*        alert.addAction(AlertAction(title: "Do something else"))

        let disabledAction = AlertAction(title: "Disabled action", style: .Default)
        disabledAction.enabled = false
        alert.addAction(disabledAction)*/
        
      
        alert.show()
    }
    
    @IBAction func showSystemAlert() {
        let alertController = UIAlertController(title: "Choose wisely",
            message: "Really delete all of your important information?",
            preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Delete everyting", style: .destructive, handler: { action in }))
        alertController.addAction(UIAlertAction(title: "Do something else", style: .default, handler: { action in }))
        
        let disabledAction = UIAlertAction(title: "Disabled action", style: .default, handler: { action in })
        disabledAction.isEnabled = false
        alertController.addAction(disabledAction)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
}

